#!/bin/bash
shopt -s globstar
shopt -s nullglob
SERVER_NAME=$HOSTNAME
SENDER=$(whoami)
USER="noreply"
RECEIVER=$1
ENCODER=$2


#Wenn die Anzahl an übergebenen Parametern Null ist, dann will der User wahrscheinlich wissen wie das Skript funktioniert :)
if [[ $# -eq 0 ]] ; then
    echo 'Zur Nutzung bitte als Argumente in der Reihenfolge EmpfängerEmail VP8/VP9 angeben, wobei VP8 der Standard ist'
    exit 1
fi

#Wenn nur ein Parameter (die Emailadresse) übergeben wird, dann setze VP8 als Standard, ansonsten übernehme den Encoder (VP9 wird angenommen)
if [[ $# -eq 1 ]] ; then
    ENCODER='VP8'
    ENCODERSTRING1="-c:v libvpx -pass 1 -qmin 5 -qmax 35 -b:v 1400K -crf 27 -threads 8 -speed 4 -cpu-used 0  -auto-alt-ref 1 -lag-in-frames 25"
    ENCODERSTRING2="-c:v libvpx -pass 2 -b:v 1400K -crf 27 -qmin 5 -qmax 35 -threads 8 -speed 2 -cpu-used 0 -auto-alt-ref 1 -lag-in-frames 25 -c:a libopus -b:a 96k -vbr on"
else
    ENCODER=$2
    ENCODERSTRING1="-c:v libvpx-vp9 -pass 1 -b:v 2000k -pix_fmt yuv420p -crf 23 -threads 8 -speed 4 -tile-columns 6 -frame-parallel 1"
    ENCODERSTRING2="-c:v libvpx-vp9 -pass 2 -pix_fmt yuv420p -b:v 2000k -crf 23 -threads 8 -speed 2 -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -c:a libopus -b:a 96k"
fi

# Alternative 1Pass VP8: ffmpeg -hide_banner -y -i inputfile -c:v libvpx  -b:v 1000K -crf 8 -qmin 0 -qmax 50 -threads 3  -speed 2 -cpu-used 1 -auto-alt-ref 1 -lag-in-frames 25 -c:a libopus -b:a 96k -vbr on -f webm output.webm

#Check auf ffmpeg mail mediainfo fehlt
#Erweiterung um Parameter für recursive, VP8,VP9,EmpfägerEmail
# Schleife für alle .avi Dateien in den Unterverzeichnissen des akt. VZ
for f in **/*.flv **/*.avi **/*.mp4 **/*.mkv **/*.divx **/*.3gp **/*.wmv **/*.rm **/*.mov; do
 target="${f%.*}.webm"
 stereo="${f%.*}_stereo.avi"
 channelamount=$(mediainfo --Output="Audio;%Channel(s)%" "$f")
 if  [ $channelamount -gt 2 ]
    then
        echo "*******************************************************"
        echo "*** Start Konvertierung Audiospur AC3 -> StereoAAC  ***"
        echo "*** Von $f zu $stereo"
        echo "*******************************************************"
        ffmpeg -hide_banner -i "$f" -c:v copy -c:a aac -ac 2 -f avi "${stereo}"
    else stereo=$f
 fi

 echo "*******************************************************"
 echo "*** Start 1.Phase Video Konvertierung $ENCODER          ***"
 echo "*** Analyse von $opusstereo"
 echo "*******************************************************"
 # Der erste Durchgang, bereitet Quality 33 vor aber limitert auf 1400k Bps
 ffmpeg -hide_banner -y -i "${stereo}"  "$ENCODERSTRING1" -an -f webm /dev/null

 echo "*******************************************************"
 echo "*** Start 2. Phase VideoKonvertierung $ENCODER           ***"
 echo "*** Konvertierung von $stereo zu $target"
 echo "*******************************************************"

 # Der zweite Durchgang berechnet nun das Video und benennt die Zieldatei um

 ffmpeg -hide_banner -i "${stereo}" "$ENCODERSTRING2" -f webm  "${target}"
 #Und es gibt noch eine Statusmail

 RC=$?
 if [ "${RC}" -ne "0" ]; then
    echo "Es gab ein Problem , da $RC"
 else
    echo "Rückgabewert ist $RC - alles ist super"
 fi
 rm "${f%.*}_stereo.avi"
 rm ffmpeg2pass-0.log
echo "*******************************************************"
echo "*** Emailversand                                    ***"
echo "*******************************************************"

 SUBJECT="${f} auf $SERVER_NAME wurde erfolgreich konvertiert"
 sizeorig=$(stat --printf="%s" "${f}")
 sizenew=$(stat --printf="%s" "${target}") 
 PERC=$(bc <<< "scale=2; ($sizeorig - $sizenew)/$sizeorig * 100")
 MAIL="Alte Größe : $sizeorig\nNeue Größe: $sizenew\nErsparnis: $PERC Prozent"
 mail -s "$SUBJECT" $RECEIVER <<< $MAIL
done

